/**
* @abstract Library functions source file for Digital-Catalog by VizCreations
* @version 1.0
* @author VizCreations
* @copyright Copyright 2013 VizCreations
*/

/*
* GNU General Public License Statement
* This file is part of Digital-Catalog.

* Digital-Catalog is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.

* Digital-Catalog is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.

* You should have received a copy of the GNU General Public License
* along with Digital-Catalog.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <ctype.h>
#include "lib.h"

char *ltrim(char *str, int len) {
	int j = 0, k = 0;
	char *string = str;
	for(j=0,k=0; j<len; j++,k++) {
		if(str[k] == ' ') {
			++string;
		} else {
			break;
		}
	}
	return string;
}

void strip_newline(char *str, int len) {
	int i = 0;
	for(i=0; i<len; i++) {
		if(str[i] == '\n') {
			str[i] = '\0';
			break;
		}
	}
}

int log_err(char *err) {
	FILE *fp;
	fp = fopen("error_log", "a+");
	if(fp) {
		fprintf(fp, "%s\n", err);
		fclose(fp);
	} else return 0;
	return 1;
}

int get_artist_key_(char *artist_name, CAT *cat) {
	int i = 0;
	int key = -1;
	for(i=0; i<cat->art_count; i++) {
		if(strcmp(cat->artists[i].name, artist_name) == 0) {
			key = i;
			break;
		}
	}
	if(key > -1)
		cat->art_current_key = key;
	return key;
}

/**
* @abstract Add an album to the catalog.
* @return void
* @param struct Catalog pointer
* @var char* int FILE*
*/
void add_album_name_(CAT *cat, char *input) { // Just the name
	FILE *fp;
	/** Copy the input into the catalog's album struct */
	strcpy(cat->albums[cat->alb_count].name, input);
	fp = fopen("albums.txt", "a+");
	if(fp) {
		fprintf(fp, "%s\n", cat->albums[cat->alb_count].name);
		fclose(fp);
	}
		
	cat->alb_current_key = cat->alb_count;
	++cat->alb_count;
}

void add_album_artist_(CAT *cat, char *input) {
	FILE *fp;
	char albumname[55];
	char artstname[50];
	int key;

	memset(albumname, 0, sizeof(albumname));
	strcat(albumname, cat->albums[cat->alb_current_key].name);
	strcat(albumname, ".txt");
	albumname[strlen(albumname)+1] = '\0';
	fp = fopen(albumname, "w"); // Information saved into a file

	/** A check to ensure the artist doesn't exist already in the application */

	strcpy(cat->albums[cat->alb_current_key].artst.name, input);
	if(fp) {
		fprintf(fp, "Artist: %s\n", cat->albums[cat->alb_current_key].artst.name);
		fclose(fp);
		memset(artstname, 0, sizeof(artstname));
		strcat(artstname, input);
		strcat(artstname, ".txt");
		artstname[strlen(artstname)+1] = '\0';
		if(!artist_exists_(input, cat)) { // Create new artist
			strcpy(cat->artists[cat->art_count].name, input); // We use art_count because we are adding a new artist.
			cat->artists[cat->art_count].albumids[cat->artists[cat->art_count].alb_count] = cat->alb_current_key; // Assign album key to artist

			/** An entry in the global artists file */
			fp = fopen("artists.txt", "a+");
			if(fp) {
				fprintf(fp, "%s\n", cat->artists[cat->art_count].name);
				fclose(fp);
			}

			/** Create artist file */
			fp = fopen(artstname, "w");
			if(fp) {
				fprintf(fp, "Name: %s\n", input);
				fclose(fp);
			}

			cat->art_current_key = cat->art_count; // Current artist in memory
			++cat->art_count; // Increment catalog's artists count
		} else { // Start mapping. Still to be done..
			/** Get key of existing artist and map album to his album keys */
			/* key is equal to cat->art_count */
			key = get_artist_key_(input, cat);
			if(key > -1) {
				cat->artists[key].albumids[cat->artists[key].alb_count] = cat->alb_current_key; // Assign album key to artist in memory
			}

			// Current artist is the one who exists
			cat->art_current_key = key;
			/** We do not increment artist count as we already have, and are mapping */
		}
		/** Save album id to artist integer array */
		fp = fopen(artstname, "a+");
		if(fp) {
			fprintf(fp, "Album ID: %d\n", cat->alb_current_key); // Assign album key to artist in file
			fclose(fp);
		}
		// Increase album count for the current artist
		cat->artists[cat->art_current_key].alb_count++;
	}

}

void add_album_rel_date_(CAT *cat, char *input) {
	FILE *fp;
	char albumname[55];

	memset(albumname, 0, sizeof(albumname));
	strcat(albumname, cat->albums[cat->alb_current_key].name);
	strcat(albumname, ".txt");
	albumname[strlen(albumname)+1] = '\0';

	strcpy(cat->albums[cat->alb_current_key].release_date, input);
	fp = fopen(albumname, "a+");
	if(fp) {
		fprintf(fp, "Release date: %s\n", cat->albums[cat->alb_current_key].release_date);
		fclose(fp);
	}

}

int album_exists_(char *album_name, CAT *cat) {
	int i = 0;
	int exists = 0;
	for(i=0; i<cat->alb_count; i++) {
		if(strcmp(cat->albums[i].name, album_name) == 0) {
			exists = 1;
			cat->alb_current_key = i;
			break;
		}
	}
	return exists;
}

int get_album_key_(char *album_name, CAT *cat) {
	int i = 0;
	int key = -1;
	for(i=0; i<cat->alb_count; i++) {
		if(strcmp(cat->albums[i].name, album_name) == 0) {
			key = i;
			break;
		}
	}
	if(key > -1)
		cat->alb_current_key = key;
	return key;
}

int artist_exists_(char *name, CAT *cat) {
	int i = 0;
	int exists = 0;
	for(i=0; i<cat->art_count; i++) {
		if(strcmp(cat->artists[i].name, name) == 0) {
			exists = 1;
			cat->art_current_key = i;
			break;
		}
	}
	return exists;
}

void add_artist_name_(CAT *cat, char *input) {
	FILE *fp;
	char artstname[50];

	strcpy(cat->artists[cat->art_count].name, input);
	fp = fopen("artists.txt", "a+");
	if(fp) {
		fprintf(fp, "%s\n", cat->artists[cat->art_count].name);
		fclose(fp);
	}

	memset(artstname, 0, sizeof(artstname));
	strcat(artstname, input);
	strcat(artstname, ".txt");
	artstname[strlen(artstname)+1] = '\0';
	fp = fopen(artstname, "w");
	if(fp) {
		fprintf(fp, "Name: %s\n", input);
		fclose(fp);
	}

	cat->art_current_key = cat->art_count;
	++cat->art_count;
}

void add_artist_album_(CAT *cat, char *input) {
	FILE *fp;
	char sel;
	int key;
	char albumname[50];
	char artstname[50];
	char err[25];
	// Add an album to main albums and increment album count, then map albumkey to artist alb_count and increment it too.
	strcpy(cat->albums[cat->alb_count].name, input);
	strcpy(cat->albums[cat->alb_count].artst.name, cat->artists[cat->art_current_key].name);
	fp = fopen("albums.txt", "a+");
	if(fp) {
		fprintf(fp, "%s\n", cat->albums[cat->alb_count].name);
		fclose(fp);
	}

	memset(artstname, 0, sizeof(artstname));
	strcpy(artstname, cat->artists[cat->art_current_key].name);
	strcat(artstname, ".txt");
	artstname[strlen(artstname)+1] = '\0';

	// Create album file for adding information
	memset(albumname, 0, sizeof(albumname));
	strcpy(albumname, cat->albums[cat->alb_count].name);
	strcat(albumname, ".txt");
	albumname[strlen(albumname)+1] = '\0';
	fp = fopen(albumname, "w");

	if(fp) {
		fprintf(fp, "Artist: %s\n", cat->artists[cat->art_current_key].name);
		fclose(fp);

		/*strcpy(cat->artists[cat->art_count].name, input);
		cat->artists[cat->art_count].albumids[cat->artists[cat->art_count].alb_count] = cat->alb_count;
		++cat->artists[cat->art_count].alb_count;
		fp = fopen("artists.txt", "a+");
		if(fp) {
			fprintf(fp, "%s\n", cat->artists[cat->art_count].name);
			fclose(fp);
		}
		cat->art_current_key = cat->art_count;*/
		//++cat->art_count;
	}

	// Now our duty to append the Album ID to the artist's file
	//key = get_album_key_(input); // We are not yet incrementing album count to get valid key
	key = cat->alb_count; // Latest album key before incrementing is the current added album
	if(key > -1) { // We map the artist to the album in memory
		cat->artists[cat->art_current_key].albumids[cat->artists[cat->art_current_key].alb_count] = key; // Increment his albums ids in memory
		++cat->artists[cat->art_current_key].alb_count; // Increment his albums count in memory
		/** Map album to artist in file */
		fp = fopen(artstname, "a+");
		if(fp) {
			fprintf(fp, "Album ID: %d\n", key);
			fclose(fp);
		}
	} else {
		memset(err, 0, sizeof(err));
		snprintf(err, sizeof(err), "Couldn't append album %s to artist %s", input, artstname);
		log_err(err);
	}
	cat->alb_current_key = cat->alb_count; // Set current album key to latest album
	++cat->alb_count; // Increment global count of albums in memory
}

void add_artist_album_rel_date_(CAT *cat, char *input) {
	FILE *fp;
	char albumname[50];

	memset(albumname, 0, sizeof(albumname));
	strcpy(albumname, cat->albums[cat->alb_current_key].name); // We use current album, because this was set when album got created
	strcat(albumname, ".txt");
	albumname[strlen(albumname)+1] = '\0'; // End the string
	strcpy(cat->albums[cat->alb_current_key].release_date, input);
	fp = fopen(albumname, "a+");
	if(fp) {
		fprintf(fp, "Release date: %s\n", cat->albums[cat->alb_current_key].release_date);
		fclose(fp);
	}
}

int map_artist_album_(CAT *cat, char *input) {
	FILE *fp;
	char artstname[50];
	char albumname[50];
	int key;
	int artkey;

	memset(albumname, 0, sizeof(albumname));
	memset(artstname, 0, sizeof(artstname));
	artkey = cat->art_current_key;
	strcpy(artstname, cat->artists[artkey].name);
	strcat(artstname, ".txt");
	artstname[strlen(artstname)+1] = '\0';

	/** Mapping is done by having an integer array in the artist struct that contains albumids from catalog struct to have all his albums at one place. */
	key = get_album_key_(input, cat);
	strcpy(cat->albums[key].artst.name, cat->artists[artkey].name);
	cat->alb_current_key = key;

	strcpy(albumname, input);
	strcat(albumname, ".txt");
	albumname[strlen(albumname)+1] = '\0';
	if(key > -1) {
		cat->artists[cat->art_current_key].albumids[cat->artists[cat->art_current_key].alb_count] = key; // Map album to artist in memory
		++cat->artists[cat->art_current_key].alb_count;
		fp = fopen(artstname, "a+");
		if(fp) {
			fprintf(fp, "Album ID: %d\n", key); // Map album to artist in file
			fclose(fp);
		}

		fp = fopen(albumname, "w"); // Freshly create album file with new data
		if(fp) {
			fprintf(fp, "Artist: %s\n", cat->albums[key].artst.name);
			fprintf(fp, "Release date: %s\n", cat->albums[key].release_date);
			fclose(fp);
		}
	} else {
		return FALSE;
	}
	return TRUE;
}

void update_albums_file_(CAT *cat) {
	FILE *fp;
	int i;
	fp = fopen("albums.txt", "w");
	if(fp) {
		for(i=0; i<cat->alb_count; i++) {
			//if(strcmp(cat->albums[i].name, "") == 0) continue;
			if(strcmp(cat->albums[i].name, "") == 0)
				fprintf(fp, "``Empty``\n");
			else
				fprintf(fp, "%s\n", cat->albums[i].name);
		}
		fclose(fp);
	}
}

int album_key_exists_(int artkey, int albkey, CAT *cat) {
	int i = 0;
	int exists = 0;
	for(i=0; i<cat->artists[artkey].alb_count; i++) {
		if(albkey == cat->artists[artkey].albumids[i]) {
			exists = 1;
			break;
		}
	}
	return exists;
}

int get_album_artist_(int albkey, CAT *cat) {
	int artkey = 0;
	int brk = 0;
	int curartkey = -1;
	struct Artist curart;
	int i = 0, j = 0;
	for(i=0; i<cat->art_count; i++) {
		curart = cat->artists[i];
		for(j=0; j<curart.alb_count; j++) {
			if(albkey == curart.albumids[j]) {
				artkey = i;
				brk = 1;
				break;
			}
		}
		if(brk == 1) break;
	}
	return artkey;
}

void del_album_artist_(int artkey, int albkey, CAT *cat) {
	int i = 0;
	int albcount = cat->artists[artkey].alb_count;
	for(i=0; i<albcount; i++) {
		if(cat->artists[artkey].albumids[i] == albkey) {
			cat->artists[artkey].albumids[i] = -1;
			break;
		}
	}
}

void delete_album_(int albkey, CAT *cat) {
	int artkey = -1;
	char oldalbumname[MAX_INPUT];

	/** Try getting old album data to unlink that file */
	memset(oldalbumname, 0, sizeof(oldalbumname));
	strcat(oldalbumname, cat->albums[albkey].name);
	strcat(oldalbumname, ".txt");
	oldalbumname[strlen(oldalbumname)+1] = '\0';

	// TODO CODE Delete album. Make id 0 in index and text file line ``Deleted``#
	cat->albums[albkey].name[0] = '\0';
	//free(cat->albums[albkey].artst.name);
	//free(cat->albums[albkey].name);
	cat->albums[albkey].artst.name[0] = '\0';
	unlink(oldalbumname); // File with albumname and info is deleted
	// Open the albums file, check the line of album name
	artkey = get_album_artist_(albkey, cat);
	if(artkey > 0) {
		// Clear album data from artist struct
		del_album_artist_(artkey, albkey, cat);
	}
	// Clear buffer
}

void edit_album_name_(CAT *cat, int albkey, char *input) {
	char albumname[30];
	
	cat->albums[albkey].name[0] = '\0';
	strcpy(cat->albums[albkey].name, input);

	
	//unlink(oldalbumname);

}

void edit_album_artist_(CAT *cat, int albkey, char *input, int alb_edited) {
	char albumname[30];
	char oldalbumname[30];
	//int alb_edited = 0;
	int alb_deleted = FALSE;

	char artstname[50];
	int artcount = 0;
	int albcount;
	int key = -1;
	int artexists = FALSE;
	int artkey;
	//int albkey;
	FILE *fp;

	memset(albumname, 0, sizeof(albumname));
	strcat(albumname, cat->albums[albkey].name);
	strcat(albumname, ".txt");
	albumname[strlen(albumname)+1] = '\0';

	memset(oldalbumname, 0, sizeof(oldalbumname));
	strcat(oldalbumname, cat->albums[albkey].name);
	strcat(oldalbumname, ".txt");
	oldalbumname[strlen(oldalbumname)+1] = '\0';
	/** Update album information about artist */
	strcpy(cat->albums[albkey].artst.name, input);
	// Getting key using 
	artkey = get_artist_key_(input, cat);

	/** Check artist data outside album */
	if(!artist_exists_(input, cat)) { // Create new artist if not exists
		strcpy(cat->artists[cat->art_count].name, input);

	} else {
		/** Artist with name exists */
		artexists = 1;				

	}

	artcount = cat->art_count;

	/** Mapping album data to artist in memory */
	if(albkey > -1) {
		if(!album_key_exists_(artkey, albkey, cat)) {
			cat->artists[artkey].albumids[cat->artists[artkey].alb_count] = albkey;
			++cat->artists[artkey].alb_count;
		}
	}

	if(artexists == FALSE) {
		/** Inserting new artist name to artists txt file */
		fp = fopen("artists.txt", "a+"); // This info is going to be overwritten anyways
		if(fp) {
			artkey = cat->art_count;
			fprintf(fp, "%s\n", cat->artists[cat->art_count].name);
			fclose(fp);
		}
		++cat->art_count;
	}

	memset(artstname, 0, sizeof(artstname));
	strcat(artstname, input);
	strcat(artstname, ".txt");
	artstname[strlen(artstname)+1] = '\0';

	if(artexists == FALSE) { // New artist, file create fresh and append name
		fp = fopen(artstname, "w");
		if(fp) {
			fprintf(fp, "Name: %s\n", input);
			fclose(fp);
		}
	}

	/** We have to check if album key already exists for artist and do not append it if it exists. */

	if(!album_key_exists_(artkey, albkey, cat)) {
		fp = fopen(artstname, "a+");
		if(fp) {
			fprintf(fp, "Album ID: %d\n", albkey); // Assign album key to artist in file
			fclose(fp);
		}
	}


	if(alb_edited == 1) {
		fp = fopen(albumname, "w");
	} else {
		fp = fopen(oldalbumname, "w");
	}

	if(fp) {
		fprintf(fp, "Artist: %s\n", cat->albums[albkey].artst.name);
		fclose(fp);
	}

	//artcount = cat->art_count;
}

void edit_album_rel_date_(CAT *cat, int albkey, char *input, int alb_edited) {
	char albumname[30];
	char oldalbumname[30];
	//int alb_edited = 0;
	int alb_deleted = FALSE;

	//int albkey;
	FILE *fp;

	memset(albumname, 0, sizeof(albumname));
	strcat(albumname, cat->albums[albkey].name);
	strcat(albumname, ".txt");
	albumname[strlen(albumname)+1] = '\0';

	memset(oldalbumname, 0, sizeof(oldalbumname));
	strcat(oldalbumname, cat->albums[albkey].name);
	strcat(oldalbumname, ".txt");
	oldalbumname[strlen(oldalbumname)+1] = '\0';

	strcpy(cat->albums[albkey].release_date, input);
	if(alb_edited ==  1) {
		fp = fopen(albumname, "a+");
	} else {
		fp = fopen(oldalbumname, "a+");
	}

	if(fp) {
		fprintf(fp, "Release date: %s\n", cat->albums[albkey].release_date);
		fclose(fp);
	}

}

void update_artists_file_(CAT *cat) {
	FILE *fp;
	int i;
	fp = fopen("artists.txt", "w");
	if(fp) {
		for(i=0; i<cat->art_count; i++) {
			//if(strcmp(cat->artists[i].name, "") == 0) continue;
			if(strcmp(cat->artists[i].name, "") == 0)
				fprintf(fp, "``Empty``\n");
			else
				fprintf(fp, "%s\n", cat->artists[i].name);
		}
		fclose(fp);
	}
}

void delete_artist_(int artkey, CAT *cat) {
	char artstname[50];
	char oldartstname[50];
	cat->artists[artkey].name[0] = '\0'; // Clear buffer

	/** Remove physical file */
	memset(artstname, 0, sizeof(artstname));
	memset(oldartstname, 0, sizeof(oldartstname));
	strcat(oldartstname, cat->artists[artkey].name);
	strcat(oldartstname, ".txt");
	oldartstname[strlen(oldartstname)+1] = '\0';

	unlink(oldartstname);
}

void del_artist_albums_(int artkey, CAT *cat) {
	int i;
	int albkey;
	for(i=0; i<cat->artists[artkey].alb_count; i++) {
		albkey = cat->artists[artkey].albumids[i];
		strcpy(cat->albums[albkey].artst.name,"``Empty``");
		// We have to remove the artist name from the album file
	}
}

void edit_artist_(CAT *cat, int artkey, char *input, int art_deleted) {
	char artstname[50];
	char oldartstname[50];
	char albumname[50];
	FILE *fp;
	int i, j;
	int key;
	int curalbid;

	memset(artstname, 0, sizeof(artstname));
	memset(oldartstname, 0, sizeof(oldartstname));
	strcat(oldartstname, cat->artists[artkey].name);
	strcat(oldartstname, ".txt");
	oldartstname[strlen(oldartstname)+1] = '\0';

	/** Update artist names of all albums with the artist associated */
	/* This is for memory purpose */
	i = 0;
	//for(i=0; i<cat->artists[sel-1].alb_count; i++) {
	/** Update album ids in file */

	/**  */
	cat->art_current_key = artkey;

	strcat(artstname, input);
	strcat(artstname, ".txt");
	artstname[strlen(artstname)+1] = '\0';
	
	if(!artist_exists_(input, cat)) {
		// As we create new artist file name, we append the old album ids he had
		/** New artist file */
		strcpy(cat->artists[artkey].name, input);
		fp = fopen(artstname, "w");
		if(fp) {
			fprintf(fp, "Name: %s\n", input);
			while(i < cat->artists[artkey].alb_count) {
				key = -1;
				//key = i;
				key = cat->artists[artkey].albumids[i];
				if(key > -1) {
					memset(cat->albums[key].artst.name, 0, sizeof(cat->albums[key].artst.name));
					strcpy(cat->albums[key].artst.name, cat->artists[artkey].name);
					fprintf(fp, "Album ID: %d\n", key);
				}
				++i;
			}
			fclose(fp);
			//++cat->art_count; // Wrong code, we musn't increment artists, we just map his new name to memory
		}

	} else { // Artist already exists with provided name
		key = -1;
		i = 0;
		// We get key of the existing artist in cat struct list of artists
		// We append the album ids of old artist to this new artist
		key = get_artist_key_(input, cat);
		cat->art_current_key = key;
		if(key > -1) {
			fp = fopen(artstname, "a+");
			if(fp) {
				while(i < cat->artists[artkey].alb_count) {
					fprintf(fp, "Album ID: %d\n", key);
					++i;
				}
				fclose(fp);
			}
		}
	}

	if(art_deleted == FALSE) {
		//cat->artists[artkey].name[0] = '\0'; // Clear memory for that stack
		//strcpy(cat->artists[artkey].name, input); // Rewrite name
	}
	//getchar();

	/** Update album file and album data with updated artist information.
	// TODO CODE
	// Pick all albums with this artist name
	// Edit the album file

	// TODO CODE
	// Function to get all album keys of that artist

	*/
	i = 0;

	// Recreate all album files with new artist name
	while(i < cat->artists[artkey].alb_count) {
		key = -1;
		key = cat->artists[artkey].albumids[i];
		if(key > -1) {
			memset(albumname, 0, sizeof(albumname));
			strcat(albumname, cat->albums[key].name);
			strcat(albumname, ".txt");
			albumname[strlen(albumname)+1] = '\0';

			/** Update in memory */
			cat->albums[key].artst.name[0] = '\0';
			strcpy(cat->albums[key].artst.name, cat->artists[cat->art_current_key].name);

			/** Update in file */
			fp = fopen(albumname, "w");
			if(fp) {
				fprintf(fp, "Artist: %s\n", cat->artists[cat->art_current_key].name);
				fprintf(fp, "Release date: %s\n", cat->albums[key].release_date);
				fclose(fp);
			}
		}
		++i;
	}
}

int init__(CAT *cat) {
	char line[50];
	FILE *fp;
	FILE *fp2;
	char albumname[30];
	char artistline[50];
	char artstname[50];
	char artstln[50];
	char *token;
	int i;
	int loaded = FALSE;
	fp = fopen("albums.txt", "r");
	memset(line, 0, sizeof(line));

	/** If file exists, preload all variables and structs with file data */
	// Albums pre-load
	if(fp) {
		while(fgets(line, 50, fp)) {
			strip_newline(line, 50);
			if(strlen(line) > 0) {
				strcpy(cat->albums[cat->alb_count].name, line);

				memset(albumname, 0, sizeof(albumname));
				strcat(albumname, line);
				strcat(albumname, ".txt");

				albumname[strlen(albumname)+1] = '\0'; // Open album file
				fp2 = fopen(albumname, "r");
				if(fp2) {
					i = 0;
					while(fgets(artistline, 30, fp2)) { // First line is always artist information line
						if(strlen(artistline) > 0) {

							token = (char *) strtok(artistline, ":");
							if(strcmp(token, "Artist") == 0) {
								token = (char *) strtok(NULL, ":");

								if(token != NULL) {

									strip_newline(token, 30);
									token = ltrim(token, 30);
									if(strlen(token) > 0) {
										strcpy(cat->albums[cat->alb_count].artst.name, token);
									}
								}
								++i;
							} else if(strcmp(token, "Release date") == 0) {
								token = (char *) strtok(NULL, ":");

								if(token != NULL) {
									strip_newline(token, 30);
									if(strlen(token) > 0) {
										strcpy(cat->albums[cat->alb_count].release_date, token);
									}
								} // Release date loaded up successfully
								++i;
							}
						}
					}
					fclose(fp2);
				}
				cat->alb_current_key = cat->alb_count;
				++cat->alb_count;
			}
		}

		fclose(fp);
	}
	if(i > 0) loaded++;

	memset(line, 0, sizeof(line));

	// Artists pre-load
	fp = fopen("artists.txt", "r");
	if(fp) {
		while(fgets(line, 50, fp)) { // Load each artist name from file
			strip_newline(line, 50);

			if(strlen(line) > 0) {

				memset(artstname, 0, sizeof(artstname));
				strcat(artstname, line);
				strcat(artstname, ".txt");
				artstname[strlen(artstname)+1] = '\0';

				fp2 = fopen(artstname, "r"); // Try read each artist file
				if(fp2) { // If file exists and we could read
					i = 0;
					cat->artists[cat->art_count].albumids[0] = -1;
					while(fgets(artstln, 50, fp2)) { // Try read each line of artist file
						if(i > 0) { // We want line greater than 0 because first line is for artist name
							if(strlen(artstln) > 0) {
								token = (char *) strtok(artstln, ":"); // Pull out tokens from each line

								if(strcmp(token, "Album ID") == 0) {
									token = (char *) strtok(NULL, ":"); // First line will not bring a token at all
									
									if(token != NULL) {
										if(strlen(token) > 0) {
											strip_newline(token, 50);
											cat->artists[cat->art_count].albumids[i] = atoi(token); // Assign the second token which is ID
											++cat->artists[cat->art_count].alb_count;
										}
									}
								}
							}
						}
						++i;
					}
					++cat->artists[cat->art_count].alb_count;
					//cat->artists[cat->art_count].alb_count = i; // This is wrong code..
					fclose(fp2);
				}
				strcpy(cat->artists[cat->art_count].name, line);
				cat->art_current_key = cat->art_count;
				++cat->art_count;
			}
		}

		fclose(fp);

		/** Next, get the albumids of each artist and propogate in his struct information */
	}

	if(i > 0) {
		//sleep(1);
		++loaded;
	}

	return loaded;
}

