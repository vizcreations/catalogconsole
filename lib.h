/**
* @abstract Header file -> functions/structs/constants for Catalog Console by VizCreations
* @version 1.0
* @author VizCreations
* @copyright Copyright 2013 VizCreations
*/

/*
* GNU General Public License Statement
* This file is part of Digital-Catalog.

* Digital-Catalog is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.

* Digital-Catalog is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.

* You should have received a copy of the GNU General Public License
* along with Digital-Catalog.  If not, see <http://www.gnu.org/licenses/>.
*/

#define MAX_INPUT 50
#define MAX_ALB 500
#define MAX_ART 200
#define TRUE 1
#define FALSE 0

typedef struct Artist {
        int key;
        char name[50];
        char bday[20];
        char country[50];
        int age;
        int albumids[MAX_ALB]; /** We store album keys in the artist struct */ /* There needs to be function to save the k                        eys to file */
        char **album_names;
        int alb_count; /** This number keeps increasing when a new album is created as well as when an album is mapped to                         the artist */

} ARTST;

typedef struct Album {
        int key;
        char name[50];
        struct Artist artst;
        char release_date[50];
} ALBM;

typedef struct Catalog {
	char intro[256];
        int key;
        struct Album albums[MAX_ALB];
        struct Artist artists[MAX_ART];
        char storename[50];
        char open_date[50];
        int alb_count;
        int art_count;
        /* A current key of artist is prefered */
        int art_current_key;
        /* A current key of album is prefered */
        int alb_current_key;
} CAT;

char *ltrim(char *, int);

void strip_newline(char *str, int len);

int log_err(char *);

void show_albums_(CAT *);

int get_artist_key_(char *artist_name, CAT *);

void add_album_name_(CAT *, char *);

void add_album_artist_(CAT *, char *);

void add_album_rel_date_(CAT *, char *);

void show_artists_(CAT *);

int album_exists_(char *album_name, CAT *);

int get_album_key_(char *album_name, CAT *);

int artist_exists_(char *name, CAT *);

void add_artist_name_(CAT *, char *);

void add_artist_album_(CAT *, char *);

void add_artist_album_rel_date_(CAT *, char *);

int map_artist_album_(CAT *, char *);

void update_albums_file_(CAT *);

int album_key_exists_(int, int, CAT *);

void show_album_info_(int, CAT *);

void del_album_artist_(int, int, CAT *cat);

void delete_album_(int, CAT *cat);

void edit_album_name_(CAT *, int, char *);

void edit_album_artist_(CAT *, int, char *, int);

void edit_album_rel_date_(CAT *, int, char *, int);

void update_artists_file_(CAT *);

void delete_artist_(int, CAT *);

void del_artist_albums_(int, CAT *);

void show_artist_info_(int, CAT *);

void edit_artist_(CAT *, int, char *, int);

void start_(CAT *);

int init__(CAT *);

