/**
* Entry point/Main source file for Digital-Catalog by VizCreations
* @version 1.0
* @author VizCreations
* @copyright Copyright 2013 VizCreations
*/

/*
* GNU General Public License Statement
* This file is part of Digital-Catalog.

* Digital-Catalog is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.

* Digital-Catalog is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.

* You should have received a copy of the GNU General Public License
* along with Digital-Catalog.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <ctype.h>
#include "lib.h"

/** Function to log to stdout => screen */
void log_screen(char *msg) {
	if(strlen(msg) > 0) {
		printf("%s\n", msg);
	}
}

void show_album_info_(int albkey, CAT *cat) {
	char albname[50];
	char artstname[50];
	char release_dt[50];
	strcpy(albname, cat->albums[albkey].name);
	strcpy(artstname, cat->albums[albkey].artst.name);
	strcpy(release_dt, cat->albums[albkey].release_date);

	printf("Artist: %s\n", artstname);
	printf("Release date: %s\n", release_dt);
}

void show_artist_info_(int artkey, CAT *cat) {
	int i = 0;
	int key = -1;
	char albumname[40];
	printf("Name: %s\n", cat->artists[artkey].name);
	while(i < cat->artists[artkey].alb_count) { // Remember the value of albumids[] array key is in turn the key for albums
		//printf("%d-%d\n", i, cat->artists[artkey].albumids[i]);
		key = -1;
		key = cat->artists[artkey].albumids[i];
		if(key > -1) {
			memset(albumname, 0, sizeof(albumname));
			strcpy(albumname, cat->albums[key].name);
			printf("Album: %s\n", albumname);
		}
		++i;
	}

}

void show_artists_(CAT *cat) {
	int i = 0;
	char buf[40] = "";
	if(cat->art_count > 0) {
		for(i=0; i<cat->art_count; i++) {
			if(strcmp(cat->artists[i].name, "") == 0) {
				memset(buf, 0, sizeof(buf));
				strcpy(buf, "``Empty``");
			} else strcpy(buf, cat->artists[i].name);
			printf("(%d)\t%s\n", i+1, buf);
		}
		//printf("(%d)\t%s\n", 0, "Exit");
	} else {
		printf("No artists created yet!\n");
	}
}


void show_albums_(CAT *cat) {
	int i = 0;
	printf("\nAll albums:\n");
	if(cat->alb_count > 0) {
		for(i=0; i<cat->alb_count; i++) {
			if(strcmp(cat->albums[i].name, "") == 0) printf("``Empty``\n");
			else if(strcmp(cat->albums[i].name, "``Empty``") == 0) printf("``Empty``\n");
			else
				printf("(%d)\t%s: By %s\n", i+1, cat->albums[i].name, cat->albums[i].artst.name);
		}
		//printf("(%d)\t%s\n", 0, "Exit");
	} else {
		log_screen("No albums created yet!");
	}
}

void start__(CAT *cat) {
	//char sel;
	int sel;
	char op;
	sel = 1;
	int artsel = 0;
	int albsel = 0;
	int albkey = -1, artkey = -1;
	char input[MAX_INPUT];
	int alb_edited = FALSE, art_edited = FALSE;
	int alb_deleted = FALSE, art_deleted = FALSE;
	int i = 0, j = 0;
	int curalbid;
	while(sel != 0) {
		memset(input, 0, MAX_INPUT); // Set memory to 0 for input
		printf("\nChoose the following options: \n");
		printf("(%d)\t%s\n", 1, "Add albums");
		printf("(%d)\t%s\n", 2, "Add artists");
		printf("(%d)\t%s\n", 3, "List albums");
		printf("(%d)\t%s\n", 4, "List artists");
		printf("(%d)\t%s\n", 0, "Exit");

		printf("\nEnter selection: ");
		scanf("%d", &sel);
		getchar();
		//sel = toupper(sel);
		switch(sel) {
			case 1:
				printf("Add a new album (type 'exit' to quit): ");
				while(fgets(input, MAX_INPUT, stdin)) { // Loop through to grab as many albums as possible
					strip_newline(input, MAX_INPUT); // Strip out new lines from input
					if(strlen(input) > 0) { // See if user entered any input
						if(strcmp(input, "exit") != 0) { // If user didn't type 'exit', we proceed
							/** Make sure album doesn't already exist */
							if(album_exists_(input, cat) == TRUE) {
								log_screen("Album already exists with that name..");
								break;
							}

							// Call the add_album_name_() function
							add_album_name_(cat, input);

							// Ask for album artist name (optional)
							printf("Enter the artist name ('exit' to quit): ");
							memset(input, 0, MAX_INPUT);
							fgets(input, MAX_INPUT, stdin);
							strip_newline(input, MAX_INPUT);
							if(strlen(input) > 0) {
								if(strcmp(input, "exit") != 0) { // User entered
									add_album_artist_(cat, input); // Just call function

								}
							}


							// Ask for album release date (optional)
							printf("Enter the album's release date (dd-mm-yy) Type 'exit' to quit: ");
							memset(input, 0, MAX_INPUT);
							fgets(input, MAX_INPUT, stdin);
							strip_newline(input, MAX_INPUT);
							if(strlen(input) > 0) {
								if(strcmp(input, "exit") != 0) { // User entered something
									add_album_rel_date_(cat, input);
								}
							}

							printf("Album '%s' created successfully. Add another ('exit' to quit): ", cat->albums[cat->alb_current_key].name);
						} else {
							break;
						}
					} else {
						break;
					}
				}

				show_albums_(cat);
				break;
			case 2:
				printf("Add a new artist (type 'exit' to quit): ");
				while(fgets(input, MAX_INPUT, stdin)) {
					strip_newline(input, MAX_INPUT);
					if(strlen(input) > 0) {
						if(strcmp(input, "exit") != 0) {
							if(artist_exists_(input, cat) == TRUE) {
								log_screen("Artist with that name already exists!");
								break;
							}

							add_artist_name_(cat, input);

							printf("Enter an album by the artist ('exit' to quit): ");
							memset(input, 0, MAX_INPUT);
							fgets(input, MAX_INPUT, stdin);
							strip_newline(input, MAX_INPUT);
							if(strlen(input) > 0) {
								if(strcmp(input, "exit") != 0) {
									if(!album_exists_(input, cat)) {
										add_artist_album_(cat, input);

										// Ask artist album release date
										printf("Enter the album's release date (dd-mm-yy) Type 'exit' to quit: ");
										memset(input, 0, MAX_INPUT);
										fgets(input, MAX_INPUT, stdin);
										strip_newline(input, MAX_INPUT);
										if(strlen(input) > 0) {
											if(strcmp(input, "exit") != 0) {
												add_artist_album_rel_date_(cat, input);
											}
										}
									} else {
										// Album exists and ask if user wants to map album to artists
										printf("Album name already exists. Do you want to map it to this artist? (y/n): ");
										scanf("%c", &op);
										getchar();
										op = toupper(op);
										switch(op) {
											case 'Y':
												map_artist_album_(cat, input);
												break;
											default:
												break;
										}
									}
								}
							}
							printf("Artist '%s' created successfully. Add another ('exit' to quit): ", cat->artists[cat->art_current_key].name);
						} else {
							break;
						}
					} else {
						break;
					}
				}
				//add_artist_(cat);
				//start_(cat);
				break;
			case 3:
				show_albums_(cat);

				if(cat->alb_count > 0) {
					printf("Enter the album to edit: ");
					scanf("%d", &albsel);
					getchar();
					if(albsel <= cat->alb_count && albsel > 0) { // We put <= for alb count because the number we show is a numeric increment to the index
						albkey = albsel - 1;
						printf("\nEditing: %s\n", cat->albums[albsel-1].name);
						show_album_info_(albkey, cat);

						printf("\nType new name or enter 'exit' to skip or 'del' to delete: ");
						fgets(input, MAX_INPUT, stdin);
						strip_newline(input, MAX_INPUT);
						if(strlen(input) > 0) {
							if(strcmp(input, "del") == 0) {
								// DELETE
								delete_album_(albkey, cat);
								alb_deleted = TRUE;
								alb_edited = FALSE;
							} else if(strcmp(input, "exit") != 0) {
								// Start editing..
								alb_edited = TRUE;
								alb_deleted = FALSE;
								edit_album_name_(cat, (albsel-1), input);
							}

							if(alb_deleted == FALSE && alb_edited == TRUE) {
								printf("Enter the artist name ('exit' to skip): ");
								memset(input, 0, MAX_INPUT);
								fgets(input, MAX_INPUT, stdin);
								strip_newline(input, MAX_INPUT);
								if(strlen(input) > 0) {
									if(strcmp(input, "exit") != 0) {
										edit_album_artist_(cat, albkey, input, alb_edited);
									}
								}

								printf("Enter the album's release date (dd-mm-yy) Type 'exit' to skip: ");
								memset(input, 0, MAX_INPUT);
								fgets(input, MAX_INPUT, stdin);
								strip_newline(input, MAX_INPUT);
								if(strlen(input) > 0) {
									if(strcmp(input, "exit") != 0) {
										edit_album_rel_date_(cat, albkey, input, alb_edited);
									}
								}
							}
							if(alb_edited == TRUE && alb_deleted == FALSE) {
								printf("Album '%s' successfully edited!\n", cat->albums[albkey].name);
							} else if(alb_deleted == TRUE) {
								printf("Album deleted successfully!\n");
							}
						}
						update_albums_file_(cat);
					}
				}
				//edit_albums_(cat);
				//start_(cat);
				break;
			case 4:
				show_artists_(cat);
				if(cat->art_count > 0) {
					printf("Enter the artist number to edit: ");
					scanf("%d", &artsel);
					getchar();
					if(artsel <= cat->art_count && artsel > 0) { // We give <= for art count because the number shown in menu is a single numeric value more to the actual index

						artkey = artsel - 1;
						printf("\nEditing: %s\n", cat->artists[artkey].name);

						show_artist_info_(artkey, cat);
						printf("\nType new name to enter or 'exit' to quit or 'del' to delete: ");
						fgets(input, MAX_INPUT, stdin);
						strip_newline(input, MAX_INPUT);
						if(strlen(input) > 0) {
							if(strcmp(input, "del") == 0) { // DELETE
								art_deleted = TRUE;
								art_edited = FALSE;
								delete_artist_(artkey, cat);
								del_artist_albums_(artkey, cat);
							} else if(strcmp(input, "exit") != 0) { // Edit
								edit_artist_(cat, artkey, input, art_deleted);
								art_edited = TRUE;
								art_deleted = FALSE;
							}

							if(art_edited == TRUE && art_deleted == FALSE) {
								printf("Artist successfully edited to '%s'\n", input);
							} else if(art_deleted = TRUE) {
								printf("Artist deleted successfully!\n");
							}
						}
						update_artists_file_(cat);
					}
				}
				//edit_artists_(cat);
				//start_(cat);
				break;
			case 0:
				//printf("Press Enter/Return key to exit..");
				break;
			default:
				printf("Invalid selection!\n");
				break;
		}
	}
}

int main(int argc, char *argv[], char *env[]) {
	CAT catalog;
	catalog.alb_count = 0;
	catalog.art_count = 0;
	int __loaded = FALSE;

	char intro[] = "\nDigital Catalog v1.0  Copyright (C) 2013  VizCreations\nThis program comes with ABSOLUTELY NO WARRANTY;\nThis is free software, and you are welcome to redistribute it\nunder certain conditions;";
	strncpy(catalog.intro, intro, 255);

	printf("%s\n", catalog.intro);
	/* Initialize any information which is available outside the program */
	printf("\nLoading application data..\n");
	sleep(1);
	__loaded = init__(&catalog);
	/*if(__loaded != FALSE) printf("Application data loaded successfully\n");
	sleep(1);*/
	/* Start the terminal session **/
	start__(&catalog);
	return 0;
}

