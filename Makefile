# @abstract Makefile for Digital-Catalog
# @author VizCreations
# @version 1.0
# @copyright Copyright 2013 VizCreations

#
# GNU General Public License Statement
# This file is part of Digital-Catalog.

# Digital-Catalog is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# Digital-Catalog is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with Digital-Catalog.  If not, see <http://www.gnu.org/licenses/>.

#Makefile

SRC_CIL = lib.c
OBJ_CIL = lib.o

all: compile install

compile:
	gcc -c $(SRC_CIL)
	ar rcs lib.a $(OBJ_CIL)
	$(RM) *.o
	

install:
	gcc -o Catalog main.c lib.a -lc

reset:
	rm *.txt
