Digital-Catalog-For-Linux version1.0:
=====================================
Digital Catalog For Linux is a very simple shell application
designed to allow you to create albums, artists.
You can individually create albums or artists and later
map either of them. This, we feel is a little useful if you are
a music lover and have quite a big physical library.


OpenSource and upgrading the application:
=========================================
The application is open source and has GPL v3 license attached to it.
This means that you can change the program to your own name, but still
give it away for FREE.

The open ness can also benefit us by having other programmers
create their own versions, in their own platforms using our
business/application logic. There is possibility of adapting this
program to Win32 or Android.


How to compile/install:
=======================
This is the tricky part. I assume you're a good Linux pro, and know
how to install from source. Otherwise, better get a friend who knows.
When you download this package and extract it to a folder, you have
to run the 'make all' command which will compile and install.

> make all

> make reset

> make compile

> make install

All these commands are valid in the Makefile (Remember you can change it) :)
If you want to clear your application data, just run make reset


Source code and headers:
========================
The program is segregated into three files.
main.c
lib.c
lib.h

Although you see some additional files, just forget about them,
and I don't have to tell you about them, if you're a C programmer.
The structures are declared and defined in the header file, whereas
the functions are only declared in it. Functions are defined in the
lib.c source file. The main.c just calls the init() function to 
begin the program.


Feedback:
=========
The project is deployed on git at 
https://bitbucket.org/vizcreations/catalogconsole


Happy programming,
~VizCreations

Reach us at
https://www.facebook.com/pages/VizCreations/196503337046748

